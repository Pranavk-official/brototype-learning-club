#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main()
{
    char str[20],c;
    
    printf("------------------------------- Qs 1 ----------------------------------- \n");
    printf("Input a char : ");
    scanf("%c",&c);
    printf("Input a sring : ");
    scanf("%s",str);

    printf("Output char : %c \n",c);
    printf("Output string : %s \n",str);    

    printf("\n------------------------------- Qs 2 ----------------------------------- \n");

    int num1;
    float num2,sum = 0;
    printf("Input num1 , num2  : ");
    scanf("%d %f",&num1,&num2);
    sum = (float)num1+num2;
    printf("Sum : %f \n",sum);

    printf("\n------------------------------- Qs 3 ----------------------------------- \n");
    int P;
    float R,N,SI;

    printf("Enter Principal amount (P) : ");
    scanf("%d",&P);
    printf("Interest rate (R) : ");
    scanf("%f",&R);
    printf("Number of years (n) : ");
    scanf("%f",&N);

    SI=(P*R*N)/100;
    printf("Simple Interest (SI) : %f\n",SI);

    return 0;
}

/* 
1. Accept a char input from the user and display it on the console
2. Accept two inputs from the user and output its sum

            Variable    Data Type
            ----------------------
            Number 1    Integer
            Number 2    Float           
            Sum         Float

3. Write a program to find the simple interest.
    a) Program should accept 3 inputs from the user and calculate simple 
       interest for the given inputs. Formula: SI=(P*R*n)/100)

            Variable                    Data Type
            ----------------------------------------------
            Principal amount (P)        Integer
            Interest rate (R)           Float
            Number of years (n)         Float
            Simple Interest (SI)        Float
            
###################### OUTPUT ###########################

$ gcc Assignment1.c -o Assignment1
$ ./Assignment1 
------------------------------- Qs 1 ----------------------------------- 
Input a char : c
Input a sring : hello
Output char : c 
Output string : hello 

------------------------------- Qs 2 ----------------------------------- 
Input num1 , num2  : 12 45.8
Sum : 57.799999 

------------------------------- Qs 3 ----------------------------------- 
Enter Principal amount (P) : 1200
Interest rate (R) : 5.4
Number of years (n) : 2
Simple Interest (SI) : 129.600006
*/