// simple menu driven calculator using if else.

#include<stdio.h>
#include<stdlib.h>

int main()
{
	int num1,num2,choice,result;
	
	printf("Enter 2 num  \n");
	printf("Enter num 1 : ");
	scanf("%d",&num1);
	printf("Enter num 2 : ");
	scanf("%d",&num2);

	printf("\n\t1. Add \n\t2. Sub\n\t3. Multiply\n\t4. Divide\n\t5. EXIT\n\nEnter a choice 1 - 4 : ");
	scanf("%d",&choice);

	if(choice==1)
	{
		result = num1 + num2;
		printf("Result : %d",result);

	}
	else if (choice == 2)
	{
		result = num1 - num2;
		printf("Result : %d",result);
	}
	else if (choice == 3)
	{
		result = num1 * num2;
		printf("Result : %d",result);
	}
	else if (choice == 4)
	{
		result = num1 / num2;
		printf("Result : %d",result);
	}
	else
	{
		printf("\n\t INVALID CHOICE ");
	}
	return 0;
}
